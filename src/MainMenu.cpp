#include "MainMenu.hpp"
#include "GameScene.hpp"
#include "MegaTanks.hpp"
#include <LK/Button.hpp>
#include <LK/Color.hpp>
#include <LK/Text.hpp>
using namespace lk;

////////////////////////////////////////
MainMenu::MainMenu() : tankType(Tank::Red)
{
}

////////////////////////////////////////
void MainMenu::onCreate()
{
	setBackgroundColor("#2f3038");
	setCursorTexture("textures/cursor.png");
	const auto &windowSize = director->getSize();

	/// Title

	auto titleText = new Text("MEGA-TANKS", "fonts/Fatsans.ttf", 100);
	titleText->setOriginCenter();
	titleText->setPosition(windowSize.x * 0.5f, windowSize.y * 0.15);
	addChild(titleText);

	/// Choose your tank area

	auto tankPlatform = new Sprite("textures/tank-platform.png");
	tankPlatform->setOriginCenter();
	tankPlatform->setPosition(windowSize.x * 0.5, windowSize.y * 0.5);
	addChild(tankPlatform);

	float tankPlatformWidth = tankPlatform->getSize().x;
	auto nextTankButton = new Button("textures/arrow.png");
	nextTankButton->setPosition(windowSize.x * 0.5 + tankPlatformWidth * 0.7, windowSize.y * 0.5);
	nextTankButton->onClick.connect(std::bind(&MainMenu::moveTank, this, 1));
	addChild(nextTankButton);

	auto prevTankButton = new Button("textures/arrow.png");
	prevTankButton->setRotation(180);
	prevTankButton->setPosition(windowSize.x * 0.5 - tankPlatformWidth * 0.7, windowSize.y * 0.5);
	prevTankButton->onClick.connect(std::bind(&MainMenu::moveTank, this, -1));
	addChild(prevTankButton);

	tankSprite = new Sprite("textures/tank-1.png");
	tankSprite->setOriginCenter();
	tankSprite->setRotation(-90);
	tankSprite->setPosition(windowSize.x * 0.5f, windowSize.y * 0.5);
	addChild(tankSprite);

	/// Scene buttons

	auto *buttonsFont = AssetsManager::getFont("fonts/Rounded Elegance.ttf");
	const auto &buttonsColor = stringToColor("#999");

	auto playButton = new Button("Play", buttonsFont, 40);
	playButton->setPosition(windowSize.x * 0.5, windowSize.y * 0.8);
	playButton->setColor(buttonsColor);
	playButton->onMouseOver.connect(std::bind(&Button::setColor, playButton, sf::Color::White));
	playButton->onMouseLeave.connect(std::bind(&Button::setColor, playButton, buttonsColor));
	playButton->onClick.connect(std::bind(&Scene::close, this, mt::GameScene));
	addChild(playButton);

	auto settingsButton = new Button("Settings", buttonsFont, 40);
	settingsButton->setPosition(windowSize.x * 0.2, windowSize.y * 0.8);
	settingsButton->setColor(buttonsColor);
	settingsButton->onMouseOver.connect(std::bind(&Button::setColor, settingsButton, sf::Color::White));
	settingsButton->onMouseLeave.connect(std::bind(&Button::setColor, settingsButton, buttonsColor));
	addChild(settingsButton);

	auto quitButton = new Button("Quit", buttonsFont, 40);
	quitButton->setPosition(windowSize.x * 0.8, windowSize.y * 0.8);
	quitButton->setColor(buttonsColor);
	quitButton->onMouseOver.connect(std::bind(&Button::setColor, quitButton, sf::Color::White));
	quitButton->onMouseLeave.connect(std::bind(&Button::setColor, quitButton, buttonsColor));
	quitButton->onClick.connect(std::bind(&Scene::close, this, mt::CloseGame));
	addChild(quitButton);
}

////////////////////////////////////////
void MainMenu::onClose(int scene)
{
	switch (scene)
	{
		case mt::GameScene:
			directorAction.type = DirectorAction::PopPushScene;
			directorAction.scene = new GameScene(tankType);
		break;

		case mt::CloseGame:
			directorAction.type = DirectorAction::CloseGame;
		break;
	}
}

////////////////////////////////////////
void MainMenu::moveTank(int offset)
{
	tankType = Tank::Type(tankType + offset);

	if (tankType <= 0)
		tankType = Tank::Purple;

	if (tankType > 6)
		tankType = Tank::Red;

	tankSprite->setTexture("textures/tank-" + std::to_string(tankType) + ".png");
}
