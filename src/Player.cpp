#include "Player.hpp"
#include <SFML/Window/Keyboard.hpp>

////////////////////////////////////////
void Player::setTank(Tank *tank)
{
	this->tank = tank;
}

////////////////////////////////////////
void Player::update()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) tank->accelerate(Tank::Forward);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) tank->accelerate(Tank::Backward);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) tank->rotate(Tank::Right);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) tank->rotate(Tank::Left);
}
