#include "GameScene.hpp"
#include "Tank.hpp"

////////////////////////////////////////
GameScene::GameScene(Tank::Type tankType) : tankType(tankType)
{
}

////////////////////////////////////////
void GameScene::onCreate()
{
	connectToKeyPressed(sf::Keyboard::Escape, std::bind(&Scene::close, this, 0));

	auto backgroundSprite = new Sprite("textures/background.png");
	backgroundSprite->setOriginCenter();
	backgroundSprite->setPosition(1366 * 0.5, 768 * 0.5);
	addChild(backgroundSprite);

	auto tank = new Tank(tankType);
	getCamera(Scene::defaultCamera)->setTarget(tank);
	tank->setPosition(500, 500);
	addChild(tank);
	player.setTank(tank);
}

////////////////////////////////////////
void GameScene::onUpdate(float deltaTime)
{
	player.update();
}
