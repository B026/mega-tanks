#include <LK/AssetsManager.hpp>
#include <LK/Director.hpp>
using namespace lk;

#include "Assets.h"
#include "MainMenu.hpp"

int main()
{
	AssetsManager::assetsPath = ASSETS;
	auto &director = Director::getInstance();
	director.create(sf::VideoMode::getDesktopMode(), "mega tanks", sf::Style::Fullscreen);
	director.setFramerateLimit(60);
	director.setMouseCursorVisible(false);

	return director.startGame(new MainMenu);
}
