#include "Tank.hpp"
#include <cmath>
#include <SFML/Window/Keyboard.hpp>

#define degreesToRadians(degrees) degrees * M_PI / 180

////////////////////////////////////////
Tank::Tank(Type type) : speed(0), maxSpeed(5.f), acceleration(0.2), rotationSpeed(3.f), accelerated(false), friction(acceleration * 0.5f)
{
	setTexture("textures/tank-" + std::to_string(type) + ".png");
	setAnchorPoint(0.35, 0.5);
}

////////////////////////////////////////
void Tank::onUpdate(float deltaTime)
{
	auto rotation = degreesToRadians(getRotation());
	move(cos(rotation) * speed, sin(rotation) * speed);

	/// Apply friction
	if (! accelerated)
	{
		if (speed > 0)
		{
			speed -= friction;

			if (speed < 0)
				speed = 0;
		}
		else if (speed < 0)
		{
			speed += friction;

			if (speed > 0)
				speed = 0;
		}
	}

	accelerated = false;
}

////////////////////////////////////////
void Tank::accelerate(Direction direction)
{
	if ((speed == maxSpeed && direction == Forward) || (speed == - maxSpeed * 0.5f && direction == Backward))
		return;

	speed += (direction == Forward) ? acceleration : - acceleration * 0.5f;
	accelerated = true;

	if (speed > maxSpeed && direction == Forward)
		speed = maxSpeed;
	else if (speed < - maxSpeed * 0.5f && direction == Backward)
		speed = - maxSpeed * 0.5f;
}

////////////////////////////////////////
void Tank::rotate(Direction direction)
{
	Entity::rotate((direction == Right) ? rotationSpeed : - rotationSpeed);
}
