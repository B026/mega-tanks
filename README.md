# Mega-Tanks

This is a 2d tank battle game built using LittleKnight.

## How to build it

In a terminal:

1. Clone it
2. Move into the cloned folder
3. Create a build folder: `mkdir build`
4. Move into the build folder: `cd build`
5. Run cmake: `cmake ..`
6. Make it: `make`
7. Play it: `./mega-tanks`
