#ifndef MEGA_TANKS_MAIN_MENU_HPP
#define MEGA_TANKS_MAIN_MENU_HPP

#include "Tank.hpp"

#include <LK/Scene.hpp>
using namespace lk;

class MainMenu : public Scene
{
public:
	MainMenu();
	void onCreate()	override;
	void onClose(int scene) override;

private:
	void moveTank(int offset);

	Tank::Type tankType;
	Sprite *tankSprite;
};

#endif // MEGA_TANKS_MAIN_MENU_HPP
