#ifndef MEGA_TANKS_TANK_HPP
#define MEGA_TANKS_TANK_HPP

#include <LK/Sprite.hpp>
using namespace lk;

class Tank : public Sprite
{
public:
	enum Direction
	{
		Forward,
		Backward,
		Right,
		Left
	};

	enum Type
	{
		Red = 1,
		Blue = 2,
		Yellow = 3,
		Green = 4,
		Gray = 5,
		Purple = 6
	};

	Tank(Type type);
	void onUpdate(float deltaTime) override;

	void accelerate(Direction direction = Forward);
	void rotate(Direction direction = Right);

private:
	float rotationSpeed;
	float acceleration;
	float speed;
	float maxSpeed;
	float friction;
	bool accelerated;
};

#endif // MEGA_TANKS_TANK_HPP
