#ifndef MEGA_TANKS_PLAYER_HPP
#define MEGA_TANKS_PLAYER_HPP

#include "Tank.hpp"

class Player
{
public:
	void setTank(Tank *tank);
	void update();

private:
	Tank *tank;
};

#endif // MEGA_TANKS_PLAYER_HPP
