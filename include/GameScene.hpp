#ifndef MEGA_TANKS_GAME_SCENE_HPP
#define MEGA_TANKS_GAME_SCENE_HPP

#include "Player.hpp"

#include <LK/Scene.hpp>
using namespace lk;

class GameScene : public Scene
{
public:
	GameScene(Tank::Type tankType);
	void onCreate()	override;
	void onUpdate(float deltaTime) override;

private:
	Player player;
	Tank::Type tankType;
};

#endif // MEGA_TANKS_GAME_SCENE_HPP
